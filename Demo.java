public class Demo
{

  public static void main (String[]args)
  {
    RailwayReservation objRailReservation =
      new RailwayReservation ("train", "ashok", "rajdhani", 2, "AC2");
    AirTicketReservation objAirTicketReservation =
      new AirTicketReservation ("flight", "anjana", "airINDIA", 3, "economy");
      System.out.println ("TrainTicketResrvation Details");
      System.out.println ("Transition Number:" +
 objRailReservation.getTransectionNumber ());
      System.out.println ("Category:" + objRailReservation.getCategory ());
      System.out.println ("Customer Name:" +
 objRailReservation.getCustomerName ());
      System.out.println ("Train Name:" + objRailReservation.getTrainName ());
      System.out.println ("number of tickets:" +
 objRailReservation.getNumberOfTickets ());
      System.out.println ("AirTicketReservation Details");
      System.out.println ("Transition Number:" +
 objAirTicketReservation.getTransectionNumber ());
      System.out.println ("Category:" +
 objAirTicketReservation.getCategory ());
      System.out.println ("Customer Name:" +
 objAirTicketReservation.getCustomerName ());
      System.out.println ("Train Name:" +
 objAirTicketReservation.getFlightName ());
      System.out.println ("number of tickets:" +
 objAirTicketReservation.getNumberOfTickets ());
  }
}



class RailwayReservation extends Reservation
{
  private String trainName;
  private int numberOfTickets;
  private String bookingclass;
  public RailwayReservation (String category, String customerName,
    String trainName, int numberOfTickets,
    String bookingClass)
  {
    super (category, customerName);

    this.trainName = trainName;
    this.numberOfTickets = numberOfTickets;
    this.bookingclass = bookingClass;
    calulateAmount ();
  }
  String getTrainName ()
  {
    return this.trainName;
  }
  int getNumberOfTickets ()
  {
    return this.numberOfTickets;
  }
  void calulateAmount ()
  {

    int amount = 0;
    String bClass = bookingclass;
    if (bClass.equalsIgnoreCase ("ac1"))
      {
amount = numberOfTickets * 1500;
      }
    if (bClass.equalsIgnoreCase ("ac2"))
      {
amount = numberOfTickets * 1100;
      }
    if (bClass.equalsIgnoreCase ("ac3"))
      {
amount = numberOfTickets * 700;
      }
    System.out.println ("amount for train reservation" + " " + amount);

  }
}

class AirTicketReservation extends Reservation
{

  private String flightName;
  private int numberOfTickets;
  private String bookingclass;
  public AirTicketReservation (String category, String customerName,
      String flightName, int numberOfTickets,
      String bookingClass)
  {
    super (category, customerName);

    this.flightName = flightName;
    this.numberOfTickets = numberOfTickets;
    this.bookingclass = bookingClass;
    calulateAmount ();

  }

  String getFlightName ()
  {
    return this.flightName;
  }
  int getNumberOfTickets ()
  {
    return this.numberOfTickets;
  }

  void calulateAmount ()
  {

    int amount = 0;
    // String bClass=bookingclass;
    if (bookingclass.equalsIgnoreCase ("business"))
      {
amount = numberOfTickets * 4500;
      }
    if (bookingclass.equalsIgnoreCase ("economy"))
      {
amount = numberOfTickets * 3500;
      }
    System.out.println ("amount for flight reservation" + " " + amount);
  }
}

class Reservation
{
  private static int transectionNumber = 1;
  private String category;
  private String customerName;
  public Reservation (String category, String customerName)
  {
    this.category = category;
    this.customerName = customerName;
  }
  int getTransectionNumber ()
  {
    transectionNumber = transectionNumber + 1;
    return this.transectionNumber;
  }
  String getCategory ()
  {
    return this.category;

  }
  String getCustomerName ()
  {
    return this.customerName;

  }
  boolean validateCategory (String category)
  {
    String c = category;
    if (c.equalsIgnoreCase ("train") || c.equalsIgnoreCase ("flight"))
      return true;
    else
      return false;
  }
}


